# Complete the find_second_largest function which accepts
# a list of numerical values and returns the second largest
# in the list
#
# If the list of values is empty, the function should
# return None
#
# If the list of values has only one value, the function
# should return None
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.


def find_second_largest(values):
    if len(values) <= 1:
        return None
    second_largest = [values]
    for val in values:
        val = sorted(values)
        second_largest = val[-2]
    return second_largest



print(find_second_largest([3, 4, 8, 9, 10 ,23, 3, 6, 7]))



#find_second_largest[0:1:-1]


# def find_second_largest(values):
#     max_value = max(values[0], values[1])
#     second_largest = min(values[0], values[1])
#     amount = len(values)
#     for val in range(2, amount):
#         if values[amount] > max_value:
#                 second_largest = max_value
#                 max_value = values[val]
#         elif values[amount] > second_largest and \
#             max_value != values:
#                 second_largest = values[val]
#         elif max_value == second_largest and \
#             second_largest != values[val]:
#                 second_largest = values[val]

# print(find_second_largest([1, 5, 9]))



# print(find_second_largest([1, 2, 3]))
