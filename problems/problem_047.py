# Complete the check_password function that accepts a
# single parameter, the password to check.
#
# A password is valid if it meets all of these criteria
#   * It must have at least one lowercase letter (a-z)
#   * It must have at least one uppercase letter (A-Z)
#   * It must have at least one digit (0-9)
#   * It must have at least one special character $, !, or @
#   * It must have six or more characters in it
#   * It must have twelve or fewer characters in it
#
# The string object has some methods that you may want to use,
# like ".isalpha", ".isdigit", ".isupper", and ".islower"

def check_password(password):

    digit = 0
    upper = 0
    lower = 0
    spec = 0
    for char in password:
        if char.isalpha() and char.islower():
            lower += 1
        elif char.isalpha() and char.isupper():
            upper += 1
        elif char.isdigit():
            digit += 1
        elif char == "@" or char == "$" or char == "!":
            spec += 1

    if (len(password) <= 12
        and len(password) >= 6
        and digit >= 1
        and upper >= 1
        and lower >= 1
        and spec >= 1):
        return True
    else:
        return False

print(check_password("Together!1"))
