# Complete the gear_for_day function which returns a list of
# gear needed for the day given certain criteria.
#   * If the day is not sunny and it is a workday, the list
#     needs to contain "umbrella"
#   * If it is a workday, the list needs to contain "laptop"
#   * If it is not a workday, the list needs to contain
#     "surfboard"

#doesnt work right
def gear_for_day(is_workday, is_sunny):
    if is_workday == True and is_sunny == True:
        return "umbrella"
    if is_workday == True:
        return "laptop"
    else:
        return "surfboard"

print(gear_for_day(True, False))

#works as it should
def gear_for_day(is_workday, is_sunny):
    gear = []
    if is_workday and not is_sunny:
        gear.append("umberella")
    if is_workday:
        gear.append("laptop")
    else:
        gear.append("surfboard")
    return gear

print(gear_for_day(True, False))
