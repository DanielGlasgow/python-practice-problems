# Complete the minimum_value function so that returns the
# minimum of two values.
#
# If the values are the same, return either.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def minimum_value(value1, value2):
    if value1 < value2:
        return value1
    else:
        return value2

first_number = input("Give me first number")
second_number = input("Give me second number")
print(minimum_value(first_number, second_number))
