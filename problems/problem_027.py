# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#

def max_in_list(values):
    if values == []:
        return None
    max_value = max(values)
    return max_value

print(max_in_list([4, 22, 774, 100, 44, -2]))
