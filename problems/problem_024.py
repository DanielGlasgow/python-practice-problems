# Complete the calculate_average function which accepts
# a list of numerical values and returns the average of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#
# Pseudocode is available for you

#average = sum of values / num of values

# def calculate_average(values):
#     sum = 0
#     for numbers in values:
#         sum = sum + numbers
#         calculate_average


def calculate_average(values):
    if len(values) == 0:
        return None
    average = sum(values)/len(values)
    return average

print(calculate_average([2, 6, 7, 2, 3]))
